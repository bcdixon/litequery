/*
The MIT License (MIT)

Copyright (c) 2013 B. Dixon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

!function(undefined) {
  var lq = window.lq = (function () {
    'use strict';

    var __private__ = {
      cache: {
        nextId: '0',
        e: {}
      },

      /**
       * Create an array of elements found in the document
       * based on the given css selector.
       *
       * @param  {String}  cssSelector The css selector used to select elements
       * @return {Array{Element}}
       */
      cssSelect: function (cssSelector) {
        var elemArray = [],
            nodeList,
            wildcard = '\\*',
            ID_TYPE = '#',
            CLASS_TYPE = '.',
            nodeListLength,
            prop,
            reg,
            i;

        if (cssSelector.indexOf(wildcard) < 0) {
          nodeList = document.querySelectorAll(cssSelector);
          nodeListLength = nodeList.length;
          for (i = 0; i < nodeListLength; i++) {
            elemArray.push(nodeList.item(i));
          }
        } else {
          nodeList = document.querySelectorAll('*');

          if (cssSelector.charAt(0) === ID_TYPE) {
            cssSelector = cssSelector.substring(1);
            prop = 'id';
            reg = new RegExp("^" + cssSelector.replace(wildcard, '.*') + "$");
          } else if (cssSelector.charAt(0) === CLASS_TYPE) {
            cssSelector = cssSelector.substring(1);
            prop = 'className';
            reg = new RegExp("(\\s|^)" + cssSelector.split(wildcard)[0] +
              '.*' + cssSelector.split(wildcard)[1] + "(\\s|$)");
          } else {
            cssSelector = cssSelector.toLowerCase();
            reg = new RegExp("^" + cssSelector.split(wildcard)[0] + '.*' +
              cssSelector.split(wildcard)[1] + "$/i");
          }

          nodeListLength = nodeList.length;
          for (i = 0; i < nodeListLength; i++) {
            if (nodeList.item(i)[prop] !== undefined) {
              if (nodeList.item(i)[prop].match(reg) !== null) {
                elemArray.push(nodeList[i]);
              }
            }
          }
        }

        return elemArray;
      },

      /**
       * Add the given element to the cache IFF it is
       * not already in the cache and add an id
       * property to the Element object.
       *
       * @param {Element} | {Array} element_s The element(s) to be added
       */
      addToCache: function (element_s) {
        var nextId = this.cache.nextId,
            element_sLength,
            i;

        if (element_s instanceof Array) {
          element_sLength = element_s.length;
          for (i = 0; i < element_sLength; i++) {
            if (!element_s[i].hasOwnProperty('_lqCacheId')) {
              element_s[i]._lqCacheId = nextId;
              this.cache.e[nextId + ''] = {};
              nextId++;
            }
          }
        } else {
          if (!element_s.hasOwnProperty('_lqCacheId')) {
            element_s._lqCacheId = nextId;
            this.cache.e[nextId] = {};
          }
        }

        this.cache.nextId = nextId + '';
      },

      /**
       * Get tag name of outer/first element in the html
       * string.
       *
       * @param  {String} html The html string
       * @return {String}      The tag name
       */
      getTagName: function (html) {
        return html.split('>')[0].split(' ')[0].substring(1);
      },

      /**
       * Get the inner html of a string of html.
       *
       * @param  {String} tagName The tag name of outer html
       * @param  {String} html    The entire html string
       * @return {String}         The inner html string
       */
      innerHtmlStr: function (tagName, html) {
        var outerStart = new RegExp('<' + tagName + '[^<]*>'),
            outerEnd   = '</' + tagName + '>',
            lastIndex,
            inner;

        inner = html.replace(outerStart, '');
        lastIndex = inner.lastIndexOf(outerEnd);
        if (lastIndex >= 0) {
          inner = inner.substring(0, lastIndex);
        }

        return inner;
      }
    };

    // Add hasOwnProperty for IE8 support
    Object.prototype.hasOwnProperty = Object.prototype.hasOwnProperty || function(prop) {
      return typeof this[prop] != 'undefined';
    };
    // Add hasOwnProperty for IE8 support
    Element.prototype.hasOwnProperty = Element.prototype.hasOwnProperty || Object.prototype.hasOwnProperty;

    /**
     * LiteQuery object constructor.
     *
     * @param {String} | {Element} The desired selection
     */
    var LiteQuery = function (selection) {
      this.e = [];

      if (typeof selection === 'string') {
        this.e = __private__.cssSelect(selection);
      } else if (selection instanceof Element) {
        this.e.push(selection);
      } else if (selection instanceof Array) {
        this.e = selection;
      } else if (selection instanceof Document) {
        this.e.push(selection);
      } else if (selection instanceof Window) {
        this.e.push(selection);
      } else if (selection instanceof Event) {
        this.e.push(selection.toElement);
      }

      this.length = this.e.length;

      __private__.addToCache(this.e);
    };

    LiteQuery.prototype = {
      constructor: LiteQuery,
      LQ_VERSION: '0.2.2',

      /**
       * Get a LiteQuery object based on the element
       * at the specified index.
       *
       * @param  {Number} i The index to get
       * @return {LiteQuery}    The single element LiteQuery object
       */
      at: function (i) {
        return new LiteQuery(this.e[i]);
      },

      /**
       * Get the DOM Element at the given index.
       *
       * @param  {Number}  i The index to get
       * @return {Element}       The single element LiteQuery object
       */
      get: function (i) {
        return this.e[i];
      },

      /**
       * Add class.
       *
       * @param  {String} className The class name to add
       * @return {LiteQuery}        The this object for function chaining
       */
      removeClass: function (className) {
        var classNmReg = new RegExp("(\\s|^)" + className + "(\\s|$)"),
            numElems = this.e.length,
            i;

        for (i = 0; i < numElems; i++) {
          this.e[i].className = this.e[i].className.replace(classNmReg, ' ').replace(/(^\s*)|(\s*$)/g, '');
        }

        return this;
      },

      /**
       * Add class.
       *
       * @param  {String} className The class name to add
       * @return {LiteQuery}        The this object for function chaining
       */
      addClass: function (className) {
        var numElems = this.e.length,
            i;

        this.removeClass(className);

        for (i = 0; i < numElems; i++) {
          this.e[i].className += ' ' + className;
        }

        return this;
      },

      /**
       * Get or set inline style.
       *
       * @param  {String}               property The CSS property to set/get
       * @param  {string} | {undefined} value    The value of the css property to set
       * @return {String} | {LiteQuery}
       */
      css: function (property, value) {
        var elemsLength,
            i;

        if (value) {
          if (this.e[0] !== undefined) {
            elemsLength = this.e.length;
            for (i = 0; i < elemsLength; i++) {
              this.e[i].style[property] = value;
            }
          }
          return this;
        } else {
          if (this.e[0] !== undefined) {
            // Get inline style first
            if (this.e[0].style[property]) {
              return this.e[0].style[property];
            }
            // Get other styles second
            if (window.getComputedStyle) {
              return window.getComputedStyle(this.e[0])[property];
            } else if (this.e[0].currentStyle) {
              return this.e[0].currentStyle[property];
            } else { throw 'Unsupported browser exception'; }
          }
        }
      },

      /**
       * Add event listeners.
       *
       * @param  {Event}      evt   The event
       * @param  {Function}   fctn  The callback function
       * @return {LiteQuery}        The this object for function chainging
       */
      on: function (evt, fctn) {
        var elemsLength,
            i,
            id,
            addListenerFctnName,
            evtName,
            cache = __private__.cache;

        if (Element.prototype.addEventListener) {
          addListenerFctnName = 'addEventListener';
          evtName = evt;
        } else if (Element.prototype.attachEvent) { // IE8
          addListenerFctnName = 'attachEvent';
          evtName = 'on' + evt;
        } else { throw 'Unsupported browser exception'; }

        elemsLength = this.e.length;
        for (i = 0; i < elemsLength; i++) {
          id = this.e[i]._lqCacheId;
          if (cache.e[id].handlers === undefined) {
            cache.e[id].handlers = {};
          }
          if (cache.e[id].handlers[evt] === undefined) {
            cache.e[id].handlers[evt] = [];
          }
          cache.e[id].handlers[evt].push(fctn);
          this.e[i][addListenerFctnName](evtName, fctn, false);
        }

        return this;
      },

      /**
       * Remove all event listeners of given type. This will only
       * work if you used the LiteQuery on method to add the
       * listener/handler.
       *
       * @param  {String} evt The event name
       * @return {LiteQuery}  The this object for function chaining
       */
      off: function (evt) {
        var numElems,
            id,
            numHandlers,
            removeListenerFctnName,
            evtName,
            i,
            j,
            cache = __private__.cache;

        if (Element.prototype.addEventListener) {
          removeListenerFctnName = 'removeEventListener';
          evtName = evt;
        } else if (Element.prototype.detachEvent) { // IE8
          removeListenerFctnName = 'attachEvent';
          evtName = 'on' + evt;
        } else { throw 'Unsupported browser exception'; }

        numElems = this.e.length;
        for (i = 0; i < numElems; i++) {
          id = this.e[i]._lqCacheId;
          if (cache.e[id].handlers !== undefined) {
            if (cache.e[id].handlers[evt] !== undefined) {
              numHandlers = cache.e[id].handlers[evt].length;
              for (j = 0; j < numHandlers; j++) {
                this.e[i][removeListenerFctnName](evtName, cache.e[id].handlers[evt][j]);
              }
              cache.e[id].handlers[evt] = null;
              delete cache.e[id].handlers[evt];
            }
          }
        }

        return this;
      },

      /**
       * Set or get the inner html.
       *
       * @param  {String}  innerHtml    The string html to set
       * @return {String} | {LiteQuery} The inner html as a string or
       *                                the LiteQuery object for function chaning
       */
      html: function (innerHtml) {
        var i,
            elemsLength = this.e.length;

        if (!innerHtml) {
          return this.e[0].innerHTML;
        } else {
          for (i = 0; i < elemsLength; i++) {
            this.e[i].innerHTML = innerHtml;
          }
        }

        return this;
      },

      /**
       * Get the parent element(s) of the selected
       * elements within the LiteQuery object. This
       * may return a LiteQuery object with multiple
       * elements if the selected elements within the
       * LiteQuery object have different parent
       * elements.
       *
       * @return {LiteQuery} The parent element(s) wrapped in a LiteQuery obj 
       */
      parent: function () {
        var parentElems = [],
            uniqueParents,
            i,
            elemsLength = this.e.length;

        for (i = 0; i < elemsLength; i++) {
          parentElems.push(this.e[i].parentElement);
        }

        uniqueParents = parentElems.reduce(function(prev, curr) {
          if (prev.indexOf(curr) < 0) {
            prev.push(curr);
          }
          return prev;
        }, []);

        return new LiteQuery(uniqueParents);
      },

      /**
       * Get the children of the element. Optionally
       * supply a css selector
       */
      children: function () {
        var childElems = [],
            i,
            j,
            elemsLength = this.e.length,
            nodeList,
            nodeListLength;

        for (i = 0; i < elemsLength; i++) {
          nodeList = this.e[i].children;
          nodeListLength = nodeList.length;
          for (j = 0; j < nodeListLength; j++) {
            childElems.push(nodeList.item(j));
          }
        }

        return new LiteQuery(childElems);
      },

      /**
       * Remove the selected element(s) from the
       * document.
       *
       * @return {LiteQuery}  The removed LiteQuery object for function chaining
       */
      remove: function () {
        var i,
            numElems = this.e.length,
            parent,
            id;

        for (i = 0; i < numElems; i++) {
          parent = this.e[i].parentElement;
          id = this.e[i]._lqCacheId;
          if (parent !== undefined && parent !== null) {
            parent.removeChild(this.e[i]);
          }
          __private__.cache.e[id] = null;
          delete __private__.cache.e[id];
        }

        return this;
      },

      /**
       * Append HTML as last child of selected element.
       *
       * @param  {String} html The html string to appended
       * @return {LiteQuery}   The parent LiteQuery object.
       */
      appendChild: function (html) {
        var innerHtml,
            elemsLength = this.e.length,
            i;

        for (i = 0; i < elemsLength; i++) {
          innerHtml = this.e[i].innerHTML;
          innerHtml += html;
          this.e[i].innerHTML = innerHtml;
        }

        return this;
      },

      /**
       * Append the html before the selected element.
       *
       * @param  {String} html The html to be appended
       * @return {LiteQuery}   The selected LiteQuery object.
       */
      prepend: function (html) {
        var tagName = __private__.getTagName(html),
            elemsLength = this.e.length,
            i,
            elem,
            parent;

        for (i = 0; i < elemsLength; i++) {
          parent = this.e[i].parentElement;
          elem = document.createElement(tagName);
          elem = parent.insertBefore(elem, this.e[i]);
          elem.outerHTML = html;
        }

        return this;
      },

      /**
       * Append the html after the selected element.
       *
       * @param  {String} html The html to be appended
       * @return {LiteQuery}   The selected LiteQuery object.
       */
      append: function (html) {
        var tagName = __private__.getTagName(html),
            elemsLength = this.e.length,
            i,
            elem,
            parent;

        for (i = 0; i < elemsLength; i++) {
          parent = this.e[i].parentElement;
          elem = document.createElement(tagName);
          if (parent.lastChild == this.e[i]) {
            elem = parent.appendChild(elem);
          } else {
            elem = parent.insertBefore(elem, this.e[i].nextSibling);
          }
          elem.outerHTML = html;
        }

        return this;
      },

      /**
       * Get the width of the element as a number.
       *
       * @return {Number} The width of the selected element
       */
      width: function () {
        var elem = this.e[0];

        if (elem !== undefined) {
          if (elem instanceof Window) {
            return screen.height;
          } else {
            return elem.offsetWidth;
          }
        }
      },

      /**
       * Get the height of the element as a number
       *
       * @return {Number} The height of the selected element
       */
      height: function () {
        var elem = this.e[0];

        if (elem !== undefined) {
          if (elem instanceof Window) {
            return screen.height;
          } else {
            return elem.offsetHeight;
          }
        }
      },

      /**
       * Return the index of the selected element within
       * the selected element's child node list.
       *
       * @return {Number} The index of the selected element
       */
      index: function () {
        var elem = this.e[0],
            children,
            childrenLength,
            i;

        if (elem !== undefined) {
          children = this.parent().children();
          childrenLength = children.length;
          for (i = 0; i < childrenLength; i++) {
            if (children.get(i) === elem) {
              return i;
            }
          }
        }

        return -1;
      }
    };

    return function (cssSelector) {
      return new LiteQuery(cssSelector);
    };
  })();

  /**
   * Call the given function when the DOMContentLoaded
   * event fires.
   *
   * @param {Function} fnctn The function to execute
   */
  lq.documentReady = function (fnctn) {
    document.addEventListener("DOMContentLoaded", fnctn, false);
  };
}();
